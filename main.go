package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"reflect"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

type Router struct {
	Port string
	IP   string
}

var lastRouterList []Router

func main() {

	namespace, selectorKey, srsPodMatchValue, err := LoadCfg()
	if err != nil {
		time.Sleep(time.Second * 1)
		panic(fmt.Errorf("failed to load config: %v ", err))
	}

	log.Printf("start watching pods in '%v' with selector of '%v=%v'",
		namespace, selectorKey, srsPodMatchValue)

	// creates the connection
	config, err := clientcmd.BuildConfigFromFlags("", os.Getenv("LOCAL_KUBE_CONFIG_FILE"))
	if err != nil {
		log.Fatal(err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Printf("Cannot get k8s config")
		log.Fatal(err)
	}

	configMaps, err := clientset.CoreV1().ConfigMaps(namespace).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Printf("Cannot get configmaps")
		log.Fatal(err)
	}
	var body []Router
	for _, cm := range configMaps.Items {
		if cm.GetName() == "routercfg" {
			for k, v := range cm.Data {
				body = append(body, Router{
					IP:   k,
					Port: v,
				})
			}
		}
	}

	for range time.NewTicker(time.Second * 30).C {
		getRouters(body)
	}
}

func LoadCfg() (ns string, key string, valueSRS string, err error) {

	ns = os.Getenv("NAMESPACE")
	if ns == "" {
		err = errors.New("missing 'NAMESPACE' in environment variable")
		return
	}

	key = os.Getenv("POD_SELECTOR_KEY")
	if key == "" {
		err = errors.New("missing 'POD_SELECTOR_KEY' in environment variable")
	}

	valueSRS = os.Getenv("POD_SRS_SELECTOR_VALUE")
	if valueSRS == "" {
		err = errors.New("missing 'POD_SRS_SELECTOR_VALUE' in environment variable")
	}

	return
}

func getRouters(body []Router) {

	if reflect.DeepEqual(body, lastRouterList) {
		log.Printf("touter not changed!")
		return
	} else {
		log.Printf("touter changed!")
		lastRouterList = body
		//更新nginx
		updateNginxCfg()
	}

	return
}

func updateNginxCfg() {

	// 尝试重新构造配置文件
	conf, err := genNginxCfg(lastRouterList)
	if err != nil {
		log.Printf("failed to execute template: %v", err)
		return
	}

	if err := reloadNgx(conf); err != nil {
		log.Printf("failed to reload nginx with new config: %v", err)
	} else {
		log.Printf("nginx config reloaded")
	}
}

func reloadNgx(conf string) error {

	cfgFilePath := "/etc/nginx/nginx.conf"

	err := ioutil.WriteFile(cfgFilePath, []byte(conf), 0666)

	if err != nil {
		log.Printf("failed to update config file: %v", err)
		return fmt.Errorf("failed to update config file: %v", err)
	}

	output, err := exec.Command("nginx", "-s", "reload").Output()
	if err != nil {
		log.Printf("failed to reload nginx:%v, %v", err, string(output))
		return fmt.Errorf("failed to reload nginx :%v, %v", err, string(output))
	}

	return nil

}

// 生成 Nginx 的配置文件
func genNginxCfg(routers []Router) (string, error) {

	t := template.Must(template.New("").Parse(nginxTemplate))

	var data struct {
		Routers []Router
	}

	data.Routers = routers

	var buf bytes.Buffer

	err := t.Execute(&buf, data)
	if err != nil {
		log.Printf("failed to execute template: %v", err)
		return "", fmt.Errorf("failed to execute template: %v", err)
	}

	return buf.String(), nil
}

// nginx 配置文件的 go template
var nginxTemplate = `
worker_processes 4;
daemon on;
user root;

events{
    worker_connections 10240;
}

error_log /proc/1/fd/1 error;

{{if .Routers}} 
# TCP loadbalancer 
stream{
	log_format proxy '$remote_addr [$time_local] '
                 '$protocol $status $bytes_sent $bytes_received '
                 '$session_time "$upstream_addr" '
                 '"$upstream_bytes_sent" "$upstream_bytes_received" "$upstream_connect_time"';

		{{range .Routers}}
			server{
				access_log /proc/1/fd/1 proxy;
				listen {{.Port}} udp;
				proxy_timeout 1s;
				proxy_responses 1;
				proxy_pass {{.IP}}:8083;
			}
		{{end}}
}	
{{else}}
# warning: no srs nodes found 
{{end}}
`
