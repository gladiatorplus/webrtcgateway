FROM registry.cn-beijing.aliyuncs.com/cmc/qsc:golang-1.16 as builder

ADD .  //go/src/gitlab.chinamcloud.com/xiangxiaojun-group/qsc-gateway

RUN cd /go/src/gitlab.chinamcloud.com/xiangxiaojun-group/qsc-gateway && \
    GOFLAGS=-mod=vendor  go build -v .


#
# 参考: https://gitlab.chinamcloud.com/qsc/ubuntu-nginx-rtmp-docker.git
#
FROM  registry.cn-beijing.aliyuncs.com/cmc/qsc:nginx-rtmp-0.0.4-3-g16e9e79


# 可执行程序 
COPY --from=builder /go/src/gitlab.chinamcloud.com/xiangxiaojun-group/qsc-gateway/qsc-gateway /bin/qsc-gateway 

ADD entry.shell /bin/entry.bash 
ADD init-nginx.conf  /etc/nginx/nginx.conf

ADD config  /bin/config


ENV NAMESPACE  qsc
ENV POD_SELECTOR_KEY  srs-cluster-node-type
ENV POD_SRS_SELECTOR_VALUE  qsc-instance
ENV SRS_SALT qsc-salt
ENV PORT_START 40001
ENV PORT_END  40010
ENV LOCAL_KUBE_CONFIG_FILE  /bin/config
#ENV NAMESPACE  srs-cluster
#ENV POD_SELECTOR_KEY  srs-cluster-node-type
#ENV POD_SRS_SELECTOR_VALUE  qsc-instance
#ENV PORT_START 40001
#ENV PORT_END  40010
#ENV LOCAL_KUBE_CONFIG_FILE  /bin/config

CMD ["bash","/bin/entry.bash"]
